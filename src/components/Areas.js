import React from 'react';
import leftArrow from '../images/left-arrow.png';
import portrait from '../images/portrait.jpg';
import menu from '../images/menu.png';
import search from '../images/search.png';
import {Link} from 'react-router-dom';
import headphone from '../images/headphone.png';

const Areas = () => {
    return(
        <div>
            <div className="topnav oneheight">
                <div className="afourth">
                    <img src={leftArrow} className='icon' alt="arrow"/>
                </div>
                <div className="half">
                    
                </div>
                <div className="afourth">
                    <img src={menu} className='icon right' alt="arrow"/>
                    <img src={search} className='icon right' alt="arrow"/>  
                </div>
            </div>
            <div className="widePage threefourthheight">
                <div className="topSearchBarForm">
                    <img src={search} className='icon' alt="search"/> 
                    <input type="text" className="topSearchBar" placeholder="Search for areas"/>
                </div>
            </div>
            <div className="halfheight">
                <div className="tabbar">
                    <div className="afourth">
                        <Link to='/people'>People</Link>
                    </div>
                    <div className="afourth active">
                        <Link to='/areas'>Areas</Link>
                    </div>
                    <div className="afourth">
                        <Link to='/rooms'>Rooms</Link>
                    </div>
                    <div className="afourth">
                        <a>Points</a>
                    </div>
                </div>
            </div>
            <div className="eightheight">
                <div className="oneheight greyArea">
                    <div className="widePage">
                        <h3>Check areas</h3>
                        <h5>Tap area to see it on the map</h5>
                    </div>
                    <div className="widePage">
                        <div className="iconContainer">
                            <img src={headphone} alt="headphone" className="icon right" />
                        </div>
                    </div>
                </div>
                <div className="sevenheight verticalList">
                    <div className="widePage">
                        <ul>
                            <div className="areaItems">
                                <h4>High-Focus</h4>
                                <h6>North-western wing</h6>
                                <div className="progressBar">
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle half"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressNumber">57%</div>
                                </div>
                            </div>
                            <div className="areaItems">
                                <h4>Base camp 3</h4>
                                <h6>Human Resources, PAYCC, Legal</h6>
                                <div className="progressBar">
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle half"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressNumber">57%</div>
                                </div>
                                <div className="group">
                                    <h6>Group members available</h6>
                                    <div className="group portraits">
                                        <img src={portrait} alt="portrait" className="avatar tiny" />
                                        <img src={portrait} alt="portrait" className="avatar tiny" />
                                        <img src={portrait} alt="portrait" className="avatar tiny" />
                                        <img src={portrait} alt="portrait" className="avatar tiny" />
                                        <img src={portrait} alt="portrait" className="avatar tiny" />
                                        <img src={portrait} alt="portrait" className="avatar tiny" />
                                    </div>
                                </div>
                            </div>
                            <div className="areaItems">
                                <h4>Open Collaboration</h4>
                                <div className="progressBar">
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressNumber">80%</div>
                                </div>
                                <div className="group">
                                    <h6>Group members available</h6>
                                    <div className="group portraits">
                                        <img src={portrait} alt="portrait" className="avatar tiny" />
                                        <img src={portrait} alt="portrait" className="avatar tiny" />
                                        <img src={portrait} alt="portrait" className="avatar tiny" />
                                        <img src={portrait} alt="portrait" className="avatar tiny" />
                                    </div>
                                </div>
                            </div>
                            <div className="areaItems">
                                <h4>Strategical meeting</h4>
                                <h6>Operations</h6>
                                <div className="progressBar">
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle full"></div>
                                    <div className="progressCircle half"></div>
                                    <div className="progressCircle empty"></div>
                                    <div className="progressNumber">90%</div>
                                </div>
                                <div className="group">
                                    <h6>Group members available</h6>
                                    <div className="group portraits">
                                        <img src={portrait} alt="portrait" className="avatar tiny" />
                                        <img src={portrait} alt="portrait" className="avatar tiny" />
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Areas;