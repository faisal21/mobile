import React from 'react';
import leftArrow from '../images/left-arrow.png';
import portrait from '../images/portrait.jpg';
import menu from '../images/menu.png';
import search from '../images/search.png';
import individu from '../images/individu.png';
import more from '../images/more.png';
import {Link} from 'react-router-dom';

const Groups = () => {
    return(
        <div>
            <div className="topnav oneheight">
                <div className="afourth">
                    <img src={leftArrow} className='icon' alt="arrow"/>
                </div>
                <div className="half">
                    
                </div>
                <div className="afourth">
                    <img src={menu} className='icon right' alt="arrow"/>
                    <img src={search} className='icon right' alt="arrow"/>  
                </div>
            </div> 
            <div className="halfheight">
                <div className="tabbar">
                    <div className="afourth active">
                        <Link to='/people'>People</Link>
                    </div>
                    <div className="afourth">
                        <Link to='/areas'>Areas</Link>
                    </div>
                    <div className="afourth">
                        <Link to='/rooms'>Rooms</Link>
                    </div>
                    <div className="afourth">
                        <a>Points</a>
                    </div>
                </div>
            </div>
            <div className="greyArea oneheight">
                <div className="widePage">
                    <h3>My Groups</h3>
                    <h5>12 groups created</h5>
                </div>
                <div className="widePage">
                    <div className="iconContainer">
                        <Link to='/people'>
                            <img src={individu} alt="group" className="icon right" />
                        </Link>
                    </div>
                </div>
            </div>
            <div className="sevenheight verticalList">
                <div className="widePage">
                    <div className="groupList">
                        <div className="group">
                            <img src={more} className="icon right" alt="more"/>
                            <br></br>
                            <h3>Project Managers</h3>
                            <br></br>
                            <h6>FIN, FR, PL</h6>
                            <br></br>
                            <br></br>
                            <br></br>
                            <div className="groupMembers">
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                            </div>
                            <h6>14 Participants</h6>
                        </div>
                        <div className="group">
                            <img src={more} className="icon right" alt="more"/>
                            <br></br>
                            <h3>Project Managers</h3>
                            <br></br>
                            <h6>FIN, FR, PL</h6>
                            <br></br>
                            <br></br>
                            <br></br>
                            <div className="groupMembers">
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                            </div>
                            <h6>14 Participants</h6>
                        </div>
                        <div className="group">
                            <img src={more} className="icon right" alt="more"/>
                            <br></br>
                            <h3>Project Managers</h3>
                            <br></br>
                            <h6>FIN, FR, PL</h6>
                            <br></br>
                            <br></br>
                            <br></br>
                            <div className="groupMembers">
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                            </div>
                            <h6>14 Participants</h6>
                        </div>
                        <div className="group">
                            <img src={more} className="icon right" alt="more"/>
                            <br></br>
                            <h3>Project Managers</h3>
                            <br></br>
                            <h6>FIN, FR, PL</h6>
                            <br></br>
                            <br></br>
                            <br></br>
                            <div className="groupMembers">
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                            </div>
                            <h6>14 Participants</h6>
                        </div>
                        <div className="group">
                            <img src={more} className="icon right" alt="more"/>
                            <br></br>
                            <h3>Project Managers</h3>
                            <br></br>
                            <h6>FIN, FR, PL</h6>
                            <br></br>
                            <br></br>
                            <br></br>
                            <div className="groupMembers">
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                            </div>
                            <h6>14 Participants</h6>
                        </div>
                        <div className="group">
                            <img src={more} className="icon right" alt="more"/>
                            <br></br>
                            <h3>Project Managers</h3>
                            <br></br>
                            <h6>FIN, FR, PL</h6>
                            <br></br>
                            <br></br>
                            <br></br>
                            <div className="groupMembers">
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                            </div>
                            <h6>14 Participants</h6>
                        </div>
                        <div className="group">
                            <img src={more} className="icon right" alt="more"/>
                            <br></br>
                            <h3>Project Managers</h3>
                            <br></br>
                            <h6>FIN, FR, PL</h6>
                            <br></br>
                            <br></br>
                            <br></br>
                            <div className="groupMembers">
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                            </div>
                            <h6>14 Participants</h6>
                        </div>
                        <div className="group">
                            <img src={more} className="icon right" alt="more"/>
                            <br></br>
                            <h3>Project Managers</h3>
                            <br></br>
                            <h6>FIN, FR, PL</h6>
                            <br></br>
                            <br></br>
                            <br></br>
                            <div className="groupMembers">
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                            </div>
                            <h6>14 Participants</h6>
                        </div>
                        <div className="group">
                            <img src={more} className="icon right" alt="more"/>
                            <br></br>
                            <h3>Project Managers</h3>
                            <br></br>
                            <h6>FIN, FR, PL</h6>
                            <br></br>
                            <br></br>
                            <br></br>
                            <div className="groupMembers">
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                            </div>
                            <h6>14 Participants</h6>
                        </div>
                        <div className="group">
                            <img src={more} className="icon right" alt="more"/>
                            <br></br>
                            <h3>Project Managers</h3>
                            <br></br>
                            <h6>FIN, FR, PL</h6>
                            <br></br>
                            <br></br>
                            <br></br>
                            <div className="groupMembers">
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                                <img src={portrait} className="avatar tiny" alt="portrait"/>
                            </div>
                            <h6>14 Participants</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div className="halfheight greyArea">
                
            </div>
        </div>
    )
}

export default Groups;