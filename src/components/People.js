import React from 'react';
import leftArrow from '../images/left-arrow.png';
import portrait from '../images/portrait.jpg';
import menu from '../images/menu.png';
import search from '../images/search.png';
import group from '../images/team.png';
import {Link} from 'react-router-dom';

const People = () =>{
    return(
        <div>
            <div className="topnav oneheight">
                <div className="afourth">
                    <img src={leftArrow} className='icon' alt="arrow"/>
                </div>
                <div className="half">
                    
                </div>
                <div className="afourth">
                    <img src={menu} className='icon right' alt="arrow"/>
                    <img src={search} className='icon right' alt="arrow"/>  
                </div>
            </div> 
            <div className="halfheight">
                <div className="tabbar">
                    <div className="afourth active">
                        <Link to='/people'>People</Link>
                    </div>
                    <div className="afourth">
                        <Link to='/areas'>Areas</Link>
                    </div>
                    <div className="afourth">
                        <Link to='/rooms'>Rooms</Link>
                    </div>
                    <div className="afourth">
                        <a>Points</a>
                    </div>
                </div>
            </div>
            <div className="greyArea oneheight">
                <div className="widePage">
                    <h3>My colleagues</h3>
                    <h5>83 currently available</h5>
                    
                </div>
                <div className="widePage">
                    <div className="iconContainer">
                        <Link to='/groups'>
                            <img src={group} alt="group" className="icon right" />
                        </Link>
                    </div>
                </div>
            </div>
            <div className="twoheight" style={{backgroundColor:'#F4F6F8'}}>
                <div className="widePage">
                    <h5 style={{textAlign:'left'}}>MOST RECENTS</h5>
                </div>
                <div className="horizontalList">
                    <div className="items">
                        <img src={portrait} alt="portrait" className="avatar small"/>
                        <h6>Name</h6>
                    </div>
                    <div className="items">
                        <img src={portrait} alt="portrait" className="avatar small"/>
                        <h6>Name</h6>
                    </div>
                    <div className="items">
                        <img src={portrait} alt="portrait" className="avatar small"/>
                        <h6>Name</h6>
                    </div>
                    <div className="items">
                        <img src={portrait} alt="portrait" className="avatar small"/>
                        <h6>Name</h6>
                    </div>
                    <div className="items">
                        <img src={portrait} alt="portrait" className="avatar small"/>
                        <h6>Name</h6>
                    </div>
                    <div className="items">
                        <img src={portrait} alt="portrait" className="avatar small"/>
                        <h6>Name</h6>
                    </div>
                    <div className="items">
                        <img src={portrait} alt="portrait" className="avatar small"/>
                        <h6>Name</h6>
                    </div>
                    <div className="items">
                        <img src={portrait} alt="portrait" className="avatar small"/>
                        <h6>Name</h6>
                    </div>
                    <div className="items">
                        <img src={portrait} alt="portrait" className="avatar small"/>
                        <h6>Name</h6>
                    </div>
                </div>
            </div>
            <div className="widePage fiveheight">
                <ul className="fiveheight verticalList">
                    <div className="alphabetContainer">
                        <div className="alphabet">
                            A
                        </div>
                    </div>
                    <div className="page">
                        <div className="items">
                            <img src={portrait} alt="portrait" className="avatar mini" />
                            <div className="content">
                                <div className="mainContent">Aaron Singleton</div>
                                <div className="extraContent">Business Analyst</div>
                            </div>
                        </div>
                        <div className="items">
                            <img src={portrait} alt="portrait" className="avatar mini" />
                            <div className="content">
                                <div className="mainContent">Aaron Singleton</div>
                                <div className="extraContent">Business Analyst</div>
                            </div>
                        </div>
                        <div className="items">
                            <img src={portrait} alt="portrait" className="avatar mini" />
                            <div className="content">
                                <div className="mainContent">Aaron Singleton</div>
                                <div className="extraContent">Business Analyst</div>
                            </div>
                        </div>
                    </div>
                    <div className="alphabetContainer">
                        <div className="alphabet">
                            B
                        </div>
                    </div>
                    <div className="page">
                        <div className="items">
                            <img src={portrait} alt="portrait" className="avatar mini" />
                            <div className="content">
                                <div className="mainContent">Brandon Salim</div>
                                <div className="extraContent">Business Analyst</div>
                            </div>
                        </div>
                        <div className="items">
                            <img src={portrait} alt="portrait" className="avatar mini" />
                            <div className="content">
                                <div className="mainContent">Brandon Salim</div>
                                <div className="extraContent">Business Analyst</div>
                            </div>
                        </div>
                        <div className="items">
                            <img src={portrait} alt="portrait" className="avatar mini" />
                            <div className="content">
                                <div className="mainContent">Brandon Salim</div>
                                <div className="extraContent">Business Analyst</div>
                            </div>
                        </div>
                    </div>
                    <div className="alphabetContainer">
                        <div className="alphabet">
                            C
                        </div>
                    </div>
                    <div className="page">
                        <div className="items">
                            <img src={portrait} alt="portrait" className="avatar mini" />
                            <div className="content">
                                <div className="mainContent">Clara</div>
                                <div className="extraContent">Business Analyst</div>
                            </div>
                        </div>
                        <div className="items">
                            <img src={portrait} alt="portrait" className="avatar mini" />
                            <div className="content">
                                <div className="mainContent">Clara</div>
                                <div className="extraContent">Business Analyst</div>
                            </div>
                        </div>
                        <div className="items">
                            <img src={portrait} alt="portrait" className="avatar mini" />
                            <div className="content">
                                <div className="mainContent">Clara</div>
                                <div className="extraContent">Business Analyst</div>
                            </div>
                        </div>
                    </div>
                </ul>
            </div>
        </div>
    )
}

export default People;