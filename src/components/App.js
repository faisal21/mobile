import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import FirstPage from './FirstPage';
import SignInPage from './SignInPage';
import SignUpPage from './SignUpPage';
import People from './People';
import Areas from './Areas';
import Rooms from './Rooms';
import Rooms2 from './Rooms2';
import Groups from './Groups';
import '../style.css';

class App extends React.Component{
    render(){
        return(
            <Router>
                <Switch>
                    <React.Fragment>
                        <Route path='/' exact component={FirstPage} />
                        <Route path='/signin' exact component={SignInPage} />
                        <Route path='/signup' exact component={SignUpPage} />
                        <Route path='/people' exact component={People} />
                        <Route path='/areas' exact component={Areas} />
                        <Route path='/rooms' exact component={Rooms} />
                        <Route path='/rooms2' exact component={Rooms2} />
                        <Route path='/groups' exact component={Groups} />
                    </React.Fragment>
                </Switch>
            </Router>
        )
    }
}

export default App;