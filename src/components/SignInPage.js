import React from 'react';
import foto from '../images/foto.jpg';
import {Link} from 'react-router-dom';
import camera from '../images/camera.png';

const SignInPage = () => {
    return(
        <div>
            <div className="page twoheight cameraContainer">
                <div className="cameraIcon">
                    <img src={camera} className="icon" alt="camera" />
                </div>
            </div>
            <div className="page twoheight">
                <img src={foto} alt="foto" className="image avatar"/>
            </div>
            <div className="page twoheight">
                <h2>First login</h2>
                <p>Please add your avatar and provide your full name and job title</p>
            </div>
            <div className="page twoheight">
                <div className="form">
                    <input type="text" name="name" autoComplete='off' required/>
                    <label for="name" className="label-name">
                        <span className="content-name">Full Name</span>
                    </label>
                </div>
                <br></br>
                <div className="form">
                    <input type="text" name="job" autoComplete='off' required/>
                    <label for="job" className="label-name">
                        <span className="content-name">Job Title</span>
                    </label>
                </div>
            </div>
            <br></br>
            <div className="page twoheight">
                <Link to='/people' style={{textDecoration:'none'}}>
                    <button className="yellowBottomButton">Next</button>
                </Link>
                <p><Link to="#">Read more</Link> about the app</p>
            </div>
        </div>
    )
}

export default SignInPage;