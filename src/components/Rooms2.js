import React from 'react';
import leftArrow from '../images/left-arrow.png';
import menu from '../images/menu.png';
import search from '../images/search.png';
import room from '../images/room.png';
import sofa from '../images/armchair.png';
import {Link} from 'react-router-dom';

const Rooms2 = () => {
    return(
        <div>
           <div className="topnav oneheight">
                <div className="afourth">
                    <img src={leftArrow} className='icon' alt="arrow"/>
                </div>
                <div className="half">
                    
                </div>
                <div className="afourth">
                    <img src={menu} className='icon right' alt="arrow"/>
                    <img src={search} className='icon right' alt="arrow"/>  
                </div>
            </div> 
            <div className="halfheight">
                <div className="tabbar">
                    <div className="afourth">
                        <Link to='/people'>People</Link>
                    </div>
                    <div className="afourth">
                        <Link to='/areas'>Areas</Link>
                    </div>
                    <div className="afourth active">
                        <Link to='/rooms'>Rooms</Link>
                    </div>
                    <div className="afourth">
                        <a>Points</a>
                    </div>
                </div>
            </div>
            <div className="greyArea oneheight">
                <div className="widePage">
                    <h3>Find your room</h3>
                    <h5>22 rooms available</h5>
                </div>
                <div className="widePage">
                    <div className="iconContainer">
                        <Link to='/rooms'>
                            <img src={room} alt="group" className="icon right" />
                        </Link>
                    </div>
                </div>
            </div>
            <div className="sevenheight verticalList">
                <div className="roomList">
                    <div className="roomContainer small">
                        <div className="room active">
                            01
                            <div className="roomIndicator active"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>4 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer small">
                        <div className="room">
                            02
                            <div className="roomIndicator active"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>4 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer medium">
                        <div className="room">
                            03
                            <div className="roomIndicator"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>6 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer medium">
                        <div className="room active">
                            04
                            <div className="roomIndicator active"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>6 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer big">
                        <div className="room">
                            05
                            <div className="roomIndicator"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>6 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                                <h6>projector</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer big">
                        <div className="room">
                            06
                            <div className="roomIndicator active"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>6 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                                <h6>projector</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer small">
                        <div className="room active">
                            07
                            <div className="roomIndicator active"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>4 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer small">
                        <div className="room">
                            08
                            <div className="roomIndicator active"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>4 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer medium">
                        <div className="room">
                            09
                            <div className="roomIndicator"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>6 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer medium">
                        <div className="room active">
                            10
                            <div className="roomIndicator active"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>6 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer big">
                        <div className="room">
                            11
                            <div className="roomIndicator"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>6 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                                <h6>projector</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer big">
                        <div className="room">
                            12
                            <div className="roomIndicator active"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>6 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                                <h6>projector</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer small">
                        <div className="room active">
                            13
                            <div className="roomIndicator active"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>4 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer small">
                        <div className="room">
                            14
                            <div className="roomIndicator active"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>4 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer medium">
                        <div className="room">
                            15
                            <div className="roomIndicator"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>6 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer medium">
                        <div className="room active">
                            16
                            <div className="roomIndicator active"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>6 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer big">
                        <div className="room">
                            17
                            <div className="roomIndicator"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>6 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                                <h6>projector</h6>
                            </div>
                        </div>
                    </div>
                    <div className="roomContainer big">
                        <div className="room">
                            18
                            <div className="roomIndicator active"></div>
                        </div>
                        <div className="roomFacility">
                            <div>
                                <h5>6 x <img src={sofa}/></h5>
                                <h6>32" TV</h6>
                                <h6>projector</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="halfheight greyArea">
            
            </div>
        </div>
    )
}

export default Rooms2;