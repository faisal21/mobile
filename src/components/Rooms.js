import React from 'react';
import leftArrow from '../images/left-arrow.png';
import menu from '../images/menu.png';
import search from '../images/search.png';
import room2 from '../images/room2.png';
import {Link} from 'react-router-dom';

const Rooms = () => {
    return(
        <div>
           <div className="topnav oneheight">
                <div className="afourth">
                    <img src={leftArrow} className='icon' alt="arrow"/>
                </div>
                <div className="half">
                    
                </div>
                <div className="afourth">
                    <img src={menu} className='icon right' alt="arrow"/>
                    <img src={search} className='icon right' alt="arrow"/>  
                </div>
            </div> 
            <div className="halfheight">
                <div className="tabbar">
                    <div className="afourth">
                        <Link to='/people'>People</Link>
                    </div>
                    <div className="afourth">
                        <Link to='/areas'>Areas</Link>
                    </div>
                    <div className="afourth active">
                        <Link to='/rooms'>Rooms</Link>
                    </div>
                    <div className="afourth">
                        <a>Points</a>
                    </div>
                </div>
            </div>
            <div className="greyArea oneheight">
                <div className="widePage">
                    <h3>Find your room</h3>
                    <h5>22 rooms available</h5>
                </div>
                <div className="widePage">
                    <div className="iconContainer">
                        <Link to='/rooms2'>
                            <img src={room2} alt="group" className="icon right" />
                        </Link>
                    </div>
                </div>
            </div>
            <div className="sevenheight verticalList">
                <div className="roomList">
                    <div className="room active">
                        01
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        02
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        03
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room active">
                        04
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        05
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room">
                        06
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room active">
                        07
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room">
                        08
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room active">
                        09
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        10
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        11
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room active">
                        12
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        13
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room">
                        14
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room active">
                        15
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room">
                        16
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room active">
                        17
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        18
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        19
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room active">
                        20
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        21
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room">
                        22
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room active">
                        23
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room">
                        24
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room active">
                        25
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        26
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        27
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room active">
                        28
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        29
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room">
                        30
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room active">
                        31
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room">
                        32
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        33
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room active">
                        34
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        35
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        36
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room active">
                        37
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        38
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room">
                        39
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room active">
                        40
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room">
                        41
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room active">
                        42
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        43
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        44
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room active">
                        45
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room">
                        46
                        <div className="roomIndicator"></div>
                    </div>
                    <div className="room">
                        47
                        <div className="roomIndicator active"></div>
                    </div>
                    <div className="room active">
                        48
                        <div className="roomIndicator"></div>
                    </div>
                </div>                   
            </div>
            <div className="halfheight greyArea">

            </div>
        </div>
    )
}

export default Rooms;