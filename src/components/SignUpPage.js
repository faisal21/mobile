import React from 'react';
import logo from '../images/logo2.png';
import {Link} from 'react-router-dom';

const SignInPage = () => {
    return(
        <div>
            <div className="page oneheight">

            </div>
            <div className="page threeheight">
                <img src={logo} alt="foto" className="image"/>
            </div>
            <div className="page twoheight">
                <h2>Sign up</h2>
                <p>Sign up by using email address and a password for the app</p>
            </div>
            <div className="page twoheight">
                <div className="form">
                    <input type="text" name="name" autocomplete='off' required/>
                    <label for="name" className="label-name">
                        <span className="content-name">E-mail address</span>
                    </label>
                </div>
                <br></br>
                <div className="form">
                    <input type="text" name="job" autocomplete='off' required/>
                    <label for="job" className="label-name">
                        <span className="content-name">Password</span>
                    </label> 
                </div>
            </div>
            <br></br>
            <div className="page twoheight">
                <Link to='/people' style={{textDecoration:'none'}}>
                    <button className="yellowBottomButton">Create account</button>
                </Link>
                <p>Have an account? <Link to="/signin">Sign in</Link></p>
            </div>
        </div>
    )
}

export default SignInPage;