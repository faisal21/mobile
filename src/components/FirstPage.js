import React from 'react';
import logo from '../images/logo192.png';
import {Link} from 'react-router-dom';

const FirstPage = () => {
    return (
        <div>
            <div className="page twoheight">

            </div>
            <div className='page threeheight'>
                <img src={logo} alt='logo' className='image'/>        
            </div>
            <div className="page twoheight">
                <h1>
                    Hello!
                </h1>
                
            </div>
            <div className="page oneheight">
                <p>Please login by using given credentials and enjoy your workplace</p>
            </div>
            <div className="page twoheight">
                <Link to='/signin' style={{textDecoration:'none'}}>
                    <button className='yellowBottomButton'>Sign in</button>
                </Link>
                <p>New here ? <Link to="/signup">Sign up</Link></p>
            </div>
        </div>
    )
}

export default FirstPage;